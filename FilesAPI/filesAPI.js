/* ACÁ SE OBTIENEN LOS DATOS DE LOS ARCHIVOS Y SE CREAN LOS JSON
 * Y SE REALIZAN LAS OPERACIONES PARA DEVOLVERLAS AL index.js
*/
var getData = require('../DataAPI/dataAPI.js');
const fs = require('fs');
var datosDolar = [];

function crearJSON() {
    let fecha = Date.now();
    return new Promise((resolve, reject) => {
        getData().then(function (datos) {
            datosDolar = require('../promedioDolar.json');

            let json = {
                valor: datos.dolar.valor
            };
            //Agregar el valor más nuevo al inicio del json
            datosDolar.unshift(json);

            //Crear archivo con valor del dólar para los otros cálculos
            fs.writeFileSync("promedioDolar.json", JSON.stringify(datosDolar));

            //Crear archivo con todos los datos obtenidos
            fs.writeFileSync("FilesAPI/mindicador_" + fecha + ".json", JSON.stringify(datos));
            resolve('Se han actualizado correctamente los datos');
        });

    })
}

function getPromedio() {
    var suma = 0;
    var promedio = 0;

    return new Promise((resolve, reject) => {
        fs.readFile("promedioDolar.json", () => {
            datosDolar = require('../promedioDolar.json');
            for (var i = 0; i < datosDolar.length; i++) {
                if (i < 5) {
                    suma = suma + datosDolar[i].valor;
                }
            }
            promedio = suma / 5;
            resolve(`El promedio del dólar es de $${promedio}`);
        })
    });
}

function getMaxMin() {
    return new Promise((resolve, reject) => {
        datosDolar = require('../promedioDolar.json');
        var valorMax = datosDolar[0].valor;
        var valorMin = datosDolar[0].valor;
        for (var i = 0; i < datosDolar.length; i++) {
            if (valorMax < datosDolar[i].valor) {
                valorMax = datosDolar[i].valor;
            }
            if (valorMin > datosDolar[i].valor) {
                valorMin = datosDolar[i].valor;
            }
        }
        resolve(`Valor máximo: ${valorMax} \nValor mínimo: ${valorMin}`);
    })
}


module.exports = { crearJSON, getPromedio, getMaxMin };



