const readline = require('readline');
const timestamp = require('time-stamp');
var colors = require('colors');

var getInfo = require('./FilesAPI/filesAPI.js');

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function menu() {
    console.log('================== MENÚ ==================');
    console.log('¿Qué desea realizar?\n1. Actualizar datos\n2. Promedio dólar, euro y tasa de desempleo\n3. Mostrar valor actual dólar, euro y tasa de desempleo\n4. Mínimo histórico\n5. Máximo histórico\n0. Salir');
    lector.question('Escriba su opción:'.underline, opcion => {
        switch (opcion) {
            case '1':
                console.log(`Actualizando los datos del día ${timestamp('DD-MM-YYYY')} a las ${timestamp('HH:mm:ss')} ...`.bgWhite.black.bold);
                getInfo.crearJSON()
                    .then(data => {
                        console.log(data.green);
                        menu();
                    })
                    .catch(error => {
                        console.log(error);
                    });
                break;
            case '2':
                console.log(`Promedios a la fecha ${timestamp('DD-MM-YYYY')} - ${timestamp('HH:mm')}`.bgWhite.black.bold);
                getInfo.getPromedio()
                    .then(data => {
                        console.log(data.green);
                        menu();
                    })
                    .catch(error => {
                        console.log(error);
                    });
                break;
            case '3':
                console.log(`Valor actual a la fecha ${timestamp('DD-MM-YYYY')} - ${timestamp('HH:mm')}`.bgWhite.black.bold);
                break;

            case '4':
                console.log(`Valor mínimo a la fecha ${timestamp('DD-MM-YYYY')} - ${timestamp('HH:mm')}`.bgWhite.black.bold);
                getInfo.getMaxMin()
                    .then(data => {
                        console.log(data.green);
                        menu();
                    })
                    .catch(error => {
                        console.log(error);
                    });
                break;

            case '5':
                console.log(`Valor máximo a la fecha ${timestamp('DD-MM-YYYY')} - ${timestamp('HH:mm')}`.bgWhite.black.bold);
                getInfo.getMaxMin()
                    .then(data => {
                        console.log(data.green);
                        menu();
                    })
                    .catch(error => {
                        console.log(error);
                    });
                break;

            case '0':
                console.log('¡Hasta luego!'.bgWhite.black.bold);
                lector.close();
                process.exit(0);
                break;
            default:
                console.log('Ingresa una opción correcta');
                menu();
                break;
        }
    })
}

menu();